import 'dart:io';

import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'app/common/util/initializer.dart';
import 'app/common/values/strings.dart';
import 'app/common/values/styles/theme.dart';
import 'app/routes/app_pages.dart';

void main() {
  Initializer.instance.init(() {
    HttpOverrides.global = MyHttpOverrides();
    runApp(const MyApp());
  });
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      builder: (BuildContext context, Widget? child) {
        final MediaQueryData data = MediaQuery.of(context);
        return MediaQuery(
          data: data.copyWith(
            textScaleFactor: 1,
          ),
          child: child!,
        );
      },
      theme: AppTheme.theme,
      title: Strings.appName,
      debugShowCheckedModeBanner: false,
      getPages: AppPages.routes,
      initialRoute: Routes.SPLASH_PAGE,
      initialBinding: InitialBindings(),
      defaultTransition: Transition.rightToLeft,
    );
  }
}

class MyHttpOverrides extends HttpOverrides {
  @override
  HttpClient createHttpClient(SecurityContext? context) {
    return super.createHttpClient(context)
      ..badCertificateCallback =
          (X509Certificate cert, String host, int port) => true;
  }
}
