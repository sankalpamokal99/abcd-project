class UserResponse {
  String? message;
  UserData? user;
  int? statusCode;

  UserResponse({this.message, this.user, this.statusCode});

  UserResponse.fromJson(Map<String, dynamic> json) {
    message = json['message'];
    user = json['data'] != null ? UserData.fromJson(json['data']) : null;
    statusCode = json['statusCode'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['message'] = message;
    if (user != null) {
      data['data'] = user!.toJson();
    }
    data['statusCode'] = statusCode;
    return data;
  }
}

class UserData {
  String? userId;
  String? email;
  String? firstName;
  String? lastName;
  String? tenantId;
  String? tenantName;
  String? token;
  String? type;

  UserData(
      {this.userId,
      this.email,
      this.firstName,
      this.lastName,
      this.tenantId,
      this.tenantName,
      this.type,
      this.token});

  UserData.fromJson(Map<String, dynamic> json) {
    userId = json['userId'];
    email = json['email'];
    firstName = json['firstName'];
    lastName = json['lastName'];
    tenantId = json['tenantId'];
    token = json['token'];
    type = json["type"];
    tenantName = json["tenantName"];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['userId'] = userId;
    data['email'] = email;
    data['firstName'] = firstName;
    data['lastName'] = lastName;
    data['tenantId'] = tenantId;
    data['token'] = token;
    data['tenantName'] = tenantName;
    data['type'] = type;
    return data;
  }
}

class SignUpUserPostRequestModel {
  String? firstName;
  String? lastName;
  String? email;
  String? organization;
  String? password;

  SignUpUserPostRequestModel({
    this.firstName,
    this.lastName,
    this.email,
    this.organization,
    this.password,
  });

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['firstName'] = firstName;
    data['lastName'] = lastName;
    data['email'] = email;
    data['organization'] = organization;
    data['password'] = password;
    return data;
  }
}
