import 'dart:convert';

ErrorResponse errorResponseFromJson(String str) =>
    ErrorResponse.fromJson(json.decode(str));

String errorResponseToJson(ErrorResponse data) => json.encode(data.toJson());

class ErrorResponse {
  ErrorResponse({
    this.error,
    this.statusCode,
    this.message,
    this.newErrorFormat,
    this.responseTime,
  });

  bool? error;
  int? statusCode;
  String? message;
  NewErrorFormat? newErrorFormat;
  int? responseTime;

  factory ErrorResponse.fromJson(Map<String, dynamic> json) => ErrorResponse(
        error: json["error"],
        statusCode: json["status_code"],
        message: json["message"],
        newErrorFormat: json["newErrorFormat"] == null
            ? null
            : NewErrorFormat.fromJson(json["newErrorFormat"]),
        responseTime: json["responseTime"],
      );

  Map<String, dynamic> toJson() => {
        "error": error,
        "status_code": statusCode,
        "message": message,
        "newErrorFormat": newErrorFormat?.toJson(),
        "responseTime": responseTime,
      };
}

class NewErrorFormat {
  NewErrorFormat({
    this.message,
    this.forwardErrorDetails,
    this.error,
  });

  String? message;
  String? forwardErrorDetails;
  Error? error;

  factory NewErrorFormat.fromJson(Map<String, dynamic> json) => NewErrorFormat(
        message: json["message"],
        forwardErrorDetails: json["forwardErrorDetails"],
        error: json["error"] == null ? null : Error.fromJson(json["error"]),
      );

  Map<String, dynamic> toJson() => {
        "message": message,
        "forwardErrorDetails": forwardErrorDetails,
        "error": error?.toJson(),
      };
}

class Error {
  Error({
    this.message,
    this.errorCode,
    this.serviceCode,
  });

  String? message;
  int? errorCode;
  String? serviceCode;

  factory Error.fromJson(Map<String, dynamic> json) => Error(
        message: json["message"],
        errorCode: json["errorCode"],
        serviceCode: json["serviceCode"],
      );

  Map<String, dynamic> toJson() => {
        "message": message,
        "errorCode": errorCode,
        "serviceCode": serviceCode,
      };
}
