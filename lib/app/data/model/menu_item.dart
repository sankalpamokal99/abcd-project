import 'package:flutter/material.dart';

class MenuItemModel {
  IconData? icon;
  String? title;
  int? id;
  String? routeName;
  List<MenuItemModel>? subMenu;

  MenuItemModel({this.icon, this.title, this.id, this.subMenu, this.routeName});
}
