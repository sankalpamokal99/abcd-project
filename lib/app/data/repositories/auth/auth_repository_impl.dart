import 'package:abcd_enterprises/app/data/model/auth/user_response.dart';
import 'package:dartz/dartz.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';

import '../../../common/api_endpoints.dart';
import '../../../common/constants.dart';
import '../../model/auth/common_model.dart';
import '../../network/api.dart';
import '../../network/response.dart';
import 'auth_repository.dart';

class AuthRepositoryImpl extends AuthRepository {
  final ApiClient apiClient;
  AuthRepositoryImpl({required this.apiClient});

  @override
  Future<Either<String, UserResponse>> doLogin(
      String userName, String password) async {
    try {
      final Map<String, dynamic> body = {
        "email": userName,
        "password": password
      };
      final CustomResponse response = await apiClient.post(ApiEndPoints.login,
          body: body, contentType: Headers.jsonContentType);
      // Check for Error Or Success
      if (response.success) {
        /// Convert Response to Domain Object
        final responseList = response.fullResponse.data;
        final UserResponse userResponse =
            UserResponse.fromJson(responseList as Map<String, dynamic>);
        return right(userResponse);
      }
      // Return Error Message
      return left(response.errorMessage);
    } catch (e) {
      debugPrint(e.toString());
      // Return Error Message
      return left(Constants.somethingWrong);
    }
  }

  @override
  Future<Either<String, CommonModel>> logout() async {
    try {
      final Map<String, dynamic> body = {};

      final CustomResponse response = await apiClient.post(
        ApiEndPoints.logout,
        body: body,
      );
      if (response.success) {
        final responseList = response.fullResponse.data;
        final CommonModel commonModel =
            CommonModel.fromJson(responseList as Map<String, dynamic>);
        return right(commonModel);
      }
      return left(response.errorMessage);
    } catch (e) {
      return left(Constants.somethingWrong);
    }
  }
}
