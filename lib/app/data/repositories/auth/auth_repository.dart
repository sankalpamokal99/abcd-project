import 'package:abcd_enterprises/app/data/model/auth/user_response.dart';
import 'package:dartz/dartz.dart';

import '../../model/auth/common_model.dart';

abstract class AuthRepository {
  Future<Either<String, UserResponse>> doLogin(
      String userName, String password);
  Future<Either<String, CommonModel>> logout();
}
