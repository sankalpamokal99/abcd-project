import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';

class CookieStorage extends Interceptor {
  @override
  void onResponse(Response response, ResponseInterceptorHandler handler) {
    _saveCookies(response)
        .then((_) => handler.next(response))
        .catchError((e, stackTrace) {
      final err = DioError(requestOptions: response.requestOptions, error: e);
      err.stackTrace = stackTrace;
      handler.reject(err, true);
    });
  }

  _saveCookies(Response response) async {
    final List<String>? cookies = response.headers[HttpHeaders.setCookieHeader];
    cookies?.forEach((element) {
      final cookie = Cookie.fromSetCookieValue(element);
      debugPrint("cookie.name ${cookie.name}");
      debugPrint("cookie.value ${cookie.value}");
      debugPrint("cookie $cookie");
    });
  }
}
