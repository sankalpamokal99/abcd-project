class CustomResponse {
  bool success;
  dynamic fullResponse;
  String fullErrorMessage;
  String errorMessage;
  CustomResponse(
      {required this.success,
      required this.fullResponse,
      this.errorMessage = "",
      this.fullErrorMessage = ""});
}
