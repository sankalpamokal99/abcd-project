import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';

import 'package:abcd_enterprises/app/data/network/response.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';

import '../../common/constants.dart';
import '../model/error/error_response.dart';
import 'api.dart';
import 'dio_client.dart';

class ApiClientImpl extends ApiClient {
  final Dio httpClient = DioClient().dio;

  Map<String, String> _setHeaders(Map<String, String> headers) {
    // if (Storage.getToken() != null &&
    //     Storage.getToken()!.isNotEmpty &&
    //     headers.containsKey('x-access-token') == false) {
    //   headers["x-access-token"] = Storage.getToken()!;
    // }
    return headers;
  }

  @override
  Future<CustomResponse> get(String url,
      {Map<String, String>? headers, ResponseType? responseType}) async {
    try {
      final Map<String, String> headerData =
          _setHeaders(headers ?? <String, String>{});
      final Options options =
          Options(headers: headerData, responseType: responseType);
      final response = await httpClient.get(url, options: options);
      return _handleResponse(response);
    } on Exception catch (exception) {
      return _handleError(exception);
    }
  }

  @override
  Future<CustomResponse> post(String url,
      {Map<String, String>? headers,
      body,
      Function? onSendProgress,
      String contentType = Headers.jsonContentType}) async {
    try {
      final Map<String, String> headerData =
          _setHeaders(headers ?? <String, String>{});
      final Options options =
          Options(headers: headerData, contentType: contentType);
      final bodyData =
          contentType == Headers.jsonContentType ? json.encode(body) : body;
      final response = await httpClient.post(url,
          options: options,
          data: bodyData,
          onSendProgress: (val1, val2) =>
              onSendProgress != null ? onSendProgress(val1, val2) : null);
      return _handleResponse(response);
    } on Exception catch (exception) {
      return _handleError(exception);
    }
  }

  @override
  Future<CustomResponse> put(
    String url, {
    Map<String, String>? headers,
    body,
  }) async {
    try {
      final Map<String, String> headerData =
          _setHeaders(headers ?? <String, String>{});
      final Options options = Options(headers: headerData);
      final response = await httpClient.put(
        url,
        options: options,
        data: body,
      );
      return _handleResponse(response);
    } on Exception catch (exception) {
      return _handleError(exception);
    }
  }

  @override
  Future<CustomResponse> delete(String url,
      {Map<String, String>? headers, body}) async {
    try {
      final Map<String, String> headerData =
          _setHeaders(headers ?? <String, String>{});
      final Options options = Options(headers: headerData);
      final response =
          await httpClient.delete(url, options: options, data: body);
      return _handleResponse(response);
    } on Exception catch (exception) {
      return _handleError(exception);
    }
  }

  /// here we handle success response
  CustomResponse _handleResponse(Response response) {
    //final int? statusCode = response.statusCode;
    return CustomResponse(
      success: true,
      fullResponse: response,
      errorMessage: "",
    );
  }

  String fullErrorMsg = "";
  String errorMsg = Constants.somethingWrong;
  String error = Constants.somethingWrong;

  ///here we handle error response
  CustomResponse _handleError(Exception exception) {
    //ErrorType errorType = ErrorType.unknownError;
    if (exception is DioError) {
      //fullErrorMsg = exception.response?.data ?? exception.toString();
      if (exception.type == DioErrorType.connectTimeout) {
        //errorType = ErrorType.connectTimeout;
        errorMsg = Constants.connectionTimeout;
      } else if (exception is SocketException) {
        // errorType = ErrorType.noConnection;
        errorMsg = Constants.noConnection;
      } else if (exception.type == DioErrorType.response) {
        debugPrint("errorrr........");
        //errorType = ErrorType.response;
        if (exception.response?.data is Map) {
          debugPrint("errorrr........ if ");
          final ErrorResponse response =
              ErrorResponse.fromJson(exception.response?.data);
          getErrorMsg(response);
        } else if (exception.response?.data is Uint8List) {
          String stringDecodedFromByte = utf8.decode(exception.response?.data);
          final ErrorResponse response =
              ErrorResponse.fromJson(json.decode(stringDecodedFromByte));
          getErrorMsg(response);
        }
      } else if (exception.error is SocketException) {
        errorMsg = Constants.noConnection;
      }
      return CustomResponse(
        success: false,
        fullErrorMessage: fullErrorMsg,
        errorMessage: errorMsg,
        fullResponse: exception.response?.data,
      );
    }
    return CustomResponse(
      success: false,
      errorMessage: Constants.somethingWrong,
      fullResponse: "",
    );
  }

  void getErrorMsg(ErrorResponse response) {
    try {
      if (response.newErrorFormat!.error != null) {
        error = response.newErrorFormat?.error?.message ?? "";
        debugPrint("error1 :: = $error");
      } else {
        error = response.newErrorFormat?.message ?? "";
        debugPrint("error2 :: = $error");
      }
    } catch (e) {
      debugPrint("error12 :: = ${e.toString()}");
      fullErrorMsg = Constants.somethingWrong;
    }
    fullErrorMsg = error;
    errorMsg = error;
  }
}
