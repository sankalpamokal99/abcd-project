import 'package:abcd_enterprises/app/data/network/response.dart';
import 'package:dio/dio.dart';

abstract class ApiClient {
  Future<CustomResponse> get(String url,
      {Map<String, String> headers, ResponseType? responseType});
  Future<CustomResponse> post(
    String url, {
    Map<String, String> headers,
    body,
    Function onSendProgress,
    String contentType = Headers.jsonContentType,
  });
  Future<CustomResponse> put(
    String url, {
    Map<String, String> headers,
    body,
  });
  Future<CustomResponse> delete(String url,
      {Map<String, String> headers, body});
}
