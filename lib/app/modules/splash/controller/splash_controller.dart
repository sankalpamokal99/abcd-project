import 'dart:async';

import 'package:get/get.dart';

import '../../../routes/app_pages.dart';

class SplashController extends GetxController {
  @override
  void onInit() {
    super.onInit();
    startTimeout();
  }

  startTimeout() async {
    var duration = const Duration(seconds: 3);
    return Timer(duration, handleTimeout);
  }

  void handleTimeout() {
    Get.offAllNamed(Routes.LOGIN_PAGE);
  }
}
