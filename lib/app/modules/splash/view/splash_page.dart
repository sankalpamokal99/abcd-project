import 'package:abcd_enterprises/app/common/values/styles/app_text_style.dart';
import 'package:flutter/material.dart';

import '../../../common/values/app_colors.dart';

class SplashPage extends StatelessWidget {
  const SplashPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.kPrimaryColor,
      body: SafeArea(
        child: SizedBox(
          child: getSplashView(),
        ),
      ),
    );
  }

  Widget getSplashView() {
    return Center(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          FlutterLogo(size: 100),
          const SizedBox(
            height: 200,
          ),
          AppTextStyle.getNunitoBold(
            "Abcd Enterprises",
            30,
            AppColors.FFFFFFFF,
          )
        ],
      ),
    );
  }
}
