import 'package:abcd_enterprises/app/common/values/styles/app_text_style.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../common/values/app_colors.dart';
import '../../../common/values/styles/dimens.dart';
import '../../../common/values/styles/font_family.dart';
import '../../../data/model/menu_item.dart';
import '../../../routes/app_pages.dart';
import '../controller/main_controller.dart';

class LayoutTemplate extends GetView<MainController> {
  LayoutTemplate({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.white,
      appBar: AppBar(
        elevation: 2,
        titleSpacing: 0.0,
        automaticallyImplyLeading: true,
        centerTitle: false,
        title: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 15.0),
          child: AppTextStyle.getNunitoBold(
              'ABCD Enterprises', 16, AppColors.white),
        ),
      ),
      body: WillPopScope(
        onWillPop: _onWillPop,
        child: Navigator(
          key: Get.nestedKey(1),
          initialRoute: Routes.BOTTOM_HOME,
          onGenerateRoute: controller.onGenerateRoute,
        ),
      ),
      bottomNavigationBar: Obx(
        () => BottomNavigationBar(
          items: controller.menuList.map((MenuItemModel menuItem) {
            return BottomNavigationBarItem(
                icon: Container(
                  padding: const EdgeInsets.only(bottom: 10.0, top: 10),
                  // child: Image.asset(
                  //   menuItem.icon!,
                  //   color: (controller.currentIndex.value == menuItem.id)
                  //       ? AppColors.kPrimaryColor
                  //       : AppColors.FFB5B5B5,
                  //   height: 20,
                  //   width: 20,
                  // ),
                  child: Icon(
                    menuItem.icon!,
                  ),
                ),
                tooltip: "",
                label: menuItem.title);
          }).toList(),
          selectedItemColor: AppColors.kPrimaryColor,
          unselectedItemColor: AppColors.FFB5B5B5,
          showUnselectedLabels: true,
          unselectedLabelStyle: const TextStyle(
            fontSize: Dimens.fontSize14,
            fontFamily: FontFamily.NunitoMedium,
          ),
          selectedLabelStyle: const TextStyle(
              fontSize: Dimens.fontSize14, fontFamily: FontFamily.NunitoBold),
          currentIndex: controller.currentIndex.value,
          type: BottomNavigationBarType.fixed,
          onTap: (int index) {
            controller.changePage(index);
          },
        ),
      ),
    );
  }

  Future<bool> _onWillPop() async {
    if (controller.currentIndex.value != 0) {
      controller.changePage(0);
      return Future.value(false);
    }
    return Future.value(true);
  }
}
