import 'package:abcd_enterprises/app/modules/customers/bindings/customer_bindings.dart';
import 'package:abcd_enterprises/app/modules/customers/view/customers_page.dart';
import 'package:abcd_enterprises/app/modules/orders/bindings/orders_bindings.dart';
import 'package:abcd_enterprises/app/modules/profile/bindings/profile_bindings.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../data/model/menu_item.dart';
import '../../../routes/app_pages.dart';
import '../../home/bindings/home_bindings.dart';
import '../../home/view/home_page.dart';
import '../../orders/view/orders_page.dart';
import '../../products/bindings/products_bindings.dart';
import '../../products/view/products_page.dart';
import '../../profile/view/profile_page.dart';

class MainController extends GetxController {
  var currentIndex = 0.obs;
  final scaffoldKey = GlobalKey<ScaffoldState>();

  List<MenuItemModel> menuList = [];

  @override
  void onInit() {
    super.onInit();
    generateMenu();
  }

  generateMenu() {
    menuList = [
      MenuItemModel(
          title: "Home",
          icon: Icons.home,
          id: 0,
          routeName: Routes.BOTTOM_HOME),
      MenuItemModel(
          title: "Products",
          icon: Icons.my_library_books,
          id: 1,
          routeName: Routes.BOTTOM_PRODUCTS),
      MenuItemModel(
          title: "Orders",
          icon: Icons.shopping_cart,
          id: 2,
          routeName: Routes.BOTTOM_OREDERS),
      MenuItemModel(
          title: "Customers",
          icon: Icons.people_alt_outlined,
          id: 3,
          routeName: Routes.BOTTOM_CUSTOMERS),
      MenuItemModel(
          title: "Profile",
          icon: Icons.account_circle,
          id: 4,
          routeName: Routes.BOTTOM_PROFILE),
    ];
  }

  Route onGenerateRoute(RouteSettings settings) {
    if (settings.name == Routes.BOTTOM_HOME) {
      return GetPageRoute(
        settings: settings,
        binding: HomeBinding(),
        page: () => const HomePage(),
      );
    }
    if (settings.name == Routes.BOTTOM_PRODUCTS) {
      return GetPageRoute(
          settings: settings,
          page: () => ProductsPage(),
          binding: ProductsBinding());
    }
    if (settings.name == Routes.BOTTOM_OREDERS) {
      return GetPageRoute(
        settings: settings,
        binding: OrdersBindings(),
        page: () => const OrdersPage(),
      );
    }
    if (settings.name == Routes.BOTTOM_PROFILE) {
      return GetPageRoute(
          settings: settings,
          page: () => const ProfilePage(),
          binding: ProfileBinding());
    }
    if (settings.name == Routes.BOTTOM_CUSTOMERS) {
      return GetPageRoute(
          settings: settings,
          page: () => const CustomersPage(),
          binding: CustomersBinding());
    }
    return GetPageRoute(
      settings: settings,
      binding: HomeBinding(),
      page: () => const HomePage(),
    );
  }

  void changePage(int index, {dynamic arguments}) {
    if (currentIndex.value == index) {
      return;
    }
    currentIndex.value = menuList[index].id ?? 0;
    Get.nestedKey(1)?.currentState?.popAndPushNamed(
        menuList[index].routeName ?? "",
        arguments: arguments);
  }
}
