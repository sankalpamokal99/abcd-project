import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../common/values/app_colors.dart';
import '../../../common/values/styles/app_text_style.dart';
import '../../../style/style.dart';

class OrdersPage extends StatelessWidget {
  const OrdersPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.offWhite,
      body: SafeArea(
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: Row(
                children: [
                  Expanded(child: placeCreditOrderButton()),
                  SizedBox(
                    width: 10,
                  ),
                  Expanded(child: placeDebitOrderButton())
                ],
              ),
            ),
            Expanded(
              child: ListView.builder(
                physics: const AlwaysScrollableScrollPhysics(),
                itemCount: 10,
                itemBuilder: (context, index) {
                  return getOrdersCard(index);
                },
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget placeDebitOrderButton() {
    return Style.getButtonWithRoundedCorners(Get.width, 40.0, () {},
        AppTextStyle.getNunitoBold('Place Debit Order', 16, AppColors.FFFFFFFF),
        backgroundColor: AppColors.FFF50031,
        leftMargin: 0,
        borderColor: Colors.transparent,
        image: Icon(
          Icons.sell_outlined,
          color: AppColors.FFFFFFFF,
        ),
        rightMargin: 0);
  }

  Widget placeCreditOrderButton() {
    return Style.getButtonWithRoundedCorners(
        Get.width,
        40.0,
        () {},
        AppTextStyle.getNunitoBold(
            'Place Credit Order', 16, AppColors.FFFFFFFF),
        backgroundColor: AppColors.FF1BBB5F,
        leftMargin: 0,
        borderColor: Colors.transparent,
        image: Icon(
          Icons.add_shopping_cart,
          color: AppColors.FFFFFFFF,
        ),
        rightMargin: 0);
  }

  Widget getOrdersCard(int index) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
      child: Container(
        decoration: BoxDecoration(
          color: AppColors.FFFFFFFF,
          borderRadius: BorderRadius.all(Radius.circular(10.0)),
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.5),
              blurRadius: 10,
            ),
          ],
        ),
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(15.0),
              child: Row(
                children: [
                  CircleAvatar(
                      radius: 27,
                      backgroundColor: AppColors.kPrimaryColor,
                      child: Icon(
                        Icons.lightbulb_sharp,
                        size: 50,
                        color: AppColors.FFFFFFFF,
                      )),
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.only(left: 6.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          AppTextStyle.getNunitoBold(
                              "JSW Steel Limited", 16, AppColors.FF404040),
                          SizedBox(
                            height: 6,
                          ),
                          AppTextStyle.getNunitoRegular(
                              "+221231231231", 14, AppColors.FF808080)
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Divider(
              color: AppColors.FFDBDBDB,
              thickness: 1,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 15.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        getDetailRow('Order Number', 'OD00000001'),
                        getDetailRow('Type',
                            (index % 2 == 0) ? 'Debit Order' : 'Credit Order',
                            valueColor: (index % 2 == 0)
                                ? AppColors.FFF50031
                                : AppColors.FF1BBB5F),
                        getDetailRow('GSTIN', '1234343141332'),
                        getDetailRow('Product Name', 'Steel bars'),
                        getDetailRow('Quantity', '30 Kg'),
                        getDetailRow('Order Status',
                            (index % 2 == 0) ? 'Open' : 'Delivered'),
                        getDetailRow('Remaining Credit', 'Rs.50000'),
                        SizedBox(
                          height: 20,
                        )
                      ],
                    ),
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      GestureDetector(
                          child: Icon(
                            Icons.arrow_forward_ios,
                            color: AppColors.FF808080,
                          ),
                          onTap: () {}),
                      SizedBox(height: 55),
                      getAddStockButton()
                    ],
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget getDetailRow(String title, String value,
      {Color valueColor = AppColors.FF404040}) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 5.0),
      child: Row(
        children: [
          AppTextStyle.getNunitoRegular(title, 14, AppColors.FF808080),
          SizedBox(
            width: 5,
          ),
          Expanded(child: AppTextStyle.getNunitoBold(value, 14, valueColor)),
        ],
      ),
    );
  }

  Widget getAddStockButton() {
    return Padding(
      padding: const EdgeInsets.only(bottom: 25.0),
      child: Style.getButtonWithRoundedCorners(140, 40.0, () {},
          AppTextStyle.getNunitoBold('Change Status', 16, AppColors.FFFFFFFF),
          backgroundColor: AppColors.kPrimaryDark,
          leftMargin: 0,
          rightMargin: 0,
          borderColor: Colors.transparent),
    );
  }
}
