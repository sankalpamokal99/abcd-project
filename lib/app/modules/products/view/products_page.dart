import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../common/values/app_colors.dart';
import '../../../common/values/styles/app_text_style.dart';
import '../../../style/style.dart';

class ProductsPage extends StatelessWidget {
  const ProductsPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.offWhite,
      body: SafeArea(
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.fromLTRB(16.0, 16.0, 16.0, 16.0),
              child: getAddProductsButton(),
            ),
            Expanded(
              child: Center(
                child: AppTextStyle.getNunitoMedium(
                  'Products view coming soon!',
                  20,
                  AppColors.kPrimaryDark,
                ),
              ),
            )
            // Expanded(
            //   child: ListView.builder(
            //     physics: const AlwaysScrollableScrollPhysics(),
            //     itemCount: 10,
            //     itemBuilder: (context, index) {
            //       return getProductCard(index);
            //     },
            //   ),
            // )
          ],
        ),
      ),
    );
  }

  Widget getAddProductsButton() {
    return Style.getButtonWithRoundedCorners(Get.width, 40.0, () {},
        AppTextStyle.getNunitoBold('Add New Product', 16, AppColors.FFFFFFFF),
        backgroundColor: AppColors.kPrimaryDark,
        leftMargin: 0,
        borderColor: Colors.transparent,
        image: Icon(
          Icons.add,
          color: AppColors.FFFFFFFF,
        ),
        rightMargin: 0);
  }
}
