import 'package:flutter/material.dart';

import '../../../common/values/app_colors.dart';
import '../../../common/values/styles/app_text_style.dart';

class ProfilePage extends StatelessWidget {
  const ProfilePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.offWhite,
      body: SafeArea(
        child: Center(
          child: AppTextStyle.getNunitoMedium(
            'Profile view coming soon!',
            20,
            AppColors.kPrimaryDark,
          ),
        ),
      ),
    );
  }
}
