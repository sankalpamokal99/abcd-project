import 'package:abcd_enterprises/app/common/values/app_colors.dart';
import 'package:abcd_enterprises/app/common/values/styles/app_text_style.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../common/values/styles/font_family.dart';
import '../../../routes/app_pages.dart';
import '../../../style/style.dart';
import '../controller/login_controller.dart';

class LoginPage extends GetView<LoginController> {
  const LoginPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: AppColors.offWhite,
        body: SingleChildScrollView(
          child: Container(
            height: Get.height - MediaQuery.of(context).padding.top,
            child: getLoginView(),
          ),
        ),
      ),
    );
  }

  Widget getLoginView() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        AppTextStyle.getNunitoBold('Welcome!', 40, AppColors.kPrimaryDark),
        SizedBox(
          height: 10,
        ),
        AppTextStyle.getNunitoLight(
            'Login to Continue', 30, AppColors.kPrimaryDark),
        SizedBox(height: 15),
        GetX<LoginController>(builder: (controller) {
          return controller.showErrorMsg.value
              ? SizedBox(
                  height: 30,
                  child: getErrorMsg(controller),
                )
              : SizedBox(
                  height: 30,
                );
        }),
        Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                AppTextStyle.getNunitoRegular(
                    'Email Address', 12, AppColors.kPrimaryColor),
                SizedBox(
                  height: 8,
                ),
                getEmailTextField(16),
                getEmailErrorMsg(),
                AppTextStyle.getNunitoRegular(
                    'Password', 12, AppColors.kPrimaryColor),
                SizedBox(
                  height: 8,
                ),
                getPasswordTextField(16),
                getPasswordErrorMsg(),
                getSignInButton(16),
              ],
            )),
      ],
    );
  }

  Widget getEmailTextField(double fontSize) {
    return GetX<LoginController>(builder: (controller) {
      return Style.getInputTextField(
        'Email Address',
        TextInputType.emailAddress,
        controller.emailController.value,
        Get.width,
        fontSize: fontSize,
        prefixIcon: const Icon(
          Icons.account_circle_outlined,
          color: AppColors.FF808080,
        ),
        leftMargin: 0,
        rightMargin: 0,
        onChanged: (value) {
          if (RegExp(
                  r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
              .hasMatch(value)) {
            controller.isEmailValid.value = true;
            controller.showEmailError.value = false;
          } else if (value.isEmpty) {
            controller.isEmailValid.value = false;
            controller.showEmailError.value = false;
          } else {
            controller.isEmailValid.value = false;
            controller.showEmailError.value = true;
          }
        },
        outlineBorderColor: !controller.showEmailError.value
            ? AppColors.kPrimaryDark
            : AppColors.FFF50031,
      );
    });
  }

  Widget getPasswordTextField(double fontSize) {
    return GetX<LoginController>(builder: (controller) {
      return Style.getInputTextField('Password', TextInputType.text,
          controller.passwordController.value, Get.width,
          fontSize: fontSize,
          prefixIcon: const Icon(
            Icons.lock_outline,
            color: AppColors.FF808080,
          ),
          suffixIcon: GestureDetector(
            child: Icon(
                controller.obscureText.value
                    ? Icons.remove_red_eye_outlined
                    : Icons.remove_red_eye,
                color: AppColors.kPrimaryDark),
            onTap: () {
              controller.obscureText.value = !controller.obscureText.value;
            },
          ),
          leftMargin: 0,
          rightMargin: 0,
          obscure: controller.obscureText.value, onChanged: (value) {
        if (RegExp(
                r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!@#\$&*~]).{4,}$')
            .hasMatch(value)) {
          controller.isPasswordValid.value = true;
          controller.showPasswordError.value = false;
        } else if (value.isEmpty) {
          controller.isPasswordValid.value = false;
          controller.showPasswordError.value = false;
        } else {
          controller.isPasswordValid.value = true;
          controller.showPasswordError.value = false;
        }
      },
          outlineBorderColor: !controller.showPasswordError.value
              ? AppColors.kPrimaryDark
              : AppColors.FFF50031);
    });
  }

  Widget getSignInButton(double fontSize) {
    return GetX<LoginController>(builder: (controller) {
      return Style.getButtonWithRoundedCorners(
        Get.width,
        48.0,
        () {
          // (controller.isEmailValid.value && controller.isPasswordValid.value)
          //     ?
          Get.offAllNamed(Routes.MAIN_PAGE);
          // : null;
        },
        AppTextStyle.getNunitoBold('Sign In', 16, AppColors.FFFFFFFF),
        backgroundColor:
            (controller.isEmailValid.value && controller.isPasswordValid.value)
                ? AppColors.kPrimaryDark
                : AppColors.kPrimaryLight,
        leftMargin: 0,
        rightMargin: 0,
        borderColor:
            (controller.isEmailValid.value && controller.isPasswordValid.value)
                ? AppColors.kPrimaryDark
                : AppColors.kPrimaryLight,
      );
    });
  }

  Widget getErrorMsg(LoginController controller) {
    return Style.multiLineText(
        width: Get.width - 20,
        text: "Error",
        fontFamily: FontFamily.NunitoLight,
        color: AppColors.FFF50031,
        fontSize: 12);
  }

  Widget getEmailErrorMsg() {
    return GetX<LoginController>(builder: (controller) {
      return !controller.showEmailError.value
          ? const SizedBox(
              height: 20,
            )
          : SizedBox(
              height: 20,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Style.errorMessage('Please enter valid email'),
                ],
              ));
    });
  }

  Widget getPasswordErrorMsg() {
    return GetX<LoginController>(builder: (controller) {
      return !controller.showPasswordError.value
          ? SizedBox(height: 32)
          : SizedBox(
              height: 32,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Style.errorMessage('Please enter valid password'),
                ],
              ));
    });
  }
}
