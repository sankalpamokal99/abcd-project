import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../data/repositories/auth/auth_repository.dart';

class LoginController extends GetxController {
  final AuthRepository authRepository;

  LoginController({required this.authRepository});

  final emailController = TextEditingController().obs;
  final passwordController = TextEditingController().obs;
  RxBool obscureText = true.obs;
  RxBool isEmailValid = false.obs;
  RxBool isPasswordValid = false.obs;
  RxBool showEmailError = false.obs;
  RxBool showPasswordError = false.obs;
  RxBool showErrorMsg = false.obs;
}
