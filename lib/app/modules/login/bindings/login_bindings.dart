import 'package:get/get.dart';

import '../../../data/network/api.dart';
import '../../../data/network/api_impl.dart';
import '../../../data/repositories/auth/auth_repository.dart';
import '../../../data/repositories/auth/auth_repository_impl.dart';
import '../controller/login_controller.dart';

class LoginBinding extends Bindings {
  @override
  void dependencies() {
    Get.put<ApiClient>(ApiClientImpl());
    Get.lazyPut<AuthRepository>(
      () => AuthRepositoryImpl(apiClient: Get.find<ApiClient>()),
    );
    Get.lazyPut<LoginController>(
      () => LoginController(authRepository: Get.find<AuthRepository>()),
    );
  }
}
