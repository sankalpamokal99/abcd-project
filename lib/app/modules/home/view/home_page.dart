import 'package:abcd_enterprises/app/common/values/styles/app_text_style.dart';
import 'package:flutter/material.dart';

import '../../../common/values/app_colors.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.offWhite,
      body: SafeArea(
        child: Center(
          child: AppTextStyle.getNunitoMedium(
              'Analytical view coming soon!', 20, AppColors.kPrimaryDark),
        ),
      ),
    );
  }
}
