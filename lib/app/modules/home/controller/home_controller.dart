import 'package:get/get.dart';

class HomeController extends GetxController {
  RxBool home = false.obs;
  @override
  void onInit() {
    home.value = true;
    super.onInit();
  }
}
