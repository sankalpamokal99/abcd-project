import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../common/values/app_colors.dart';
import '../../../common/values/styles/app_text_style.dart';
import '../../../routes/app_pages.dart';
import '../../../style/style.dart';

class CustomersPage extends StatelessWidget {
  const CustomersPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.offWhite,
      body: SafeArea(
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.fromLTRB(16.0, 16.0, 16.0, 16.0),
              child: getAddNewCustomerButton(),
            ),
            Center(
              child: AppTextStyle.getNunitoMedium(
                'Customers view coming soon!',
                20,
                AppColors.kPrimaryDark,
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget getAddNewCustomerButton() {
    return Style.getButtonWithRoundedCorners(Get.width, 40.0, () {
      Get.toNamed(Routes.ADD_CUSTOMER_PAGE);
    },
        AppTextStyle.getNunitoBold(
            'Onboard New Customer', 16, AppColors.FFFFFFFF),
        backgroundColor: AppColors.kPrimaryDark,
        leftMargin: 0,
        borderColor: Colors.transparent,
        image: Icon(
          Icons.person,
          color: AppColors.FFFFFFFF,
        ),
        rightMargin: 0);
  }
}
