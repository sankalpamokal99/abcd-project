import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../common/values/app_colors.dart';
import '../../../common/values/styles/app_text_style.dart';
import '../../../style/style.dart';

class AddNewCustomer extends StatelessWidget {
  const AddNewCustomer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.offWhite,
      appBar: AppBar(
          title: AppTextStyle.getNunitoBold(
              'Onboard New Customer', 16, AppColors.white)),
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                height: 16,
              ),
              AppTextStyle.getNunitoRegular(
                  'Company Name', 14, AppColors.kPrimaryColor),
              SizedBox(
                height: 8,
              ),
              getCompanyNameTextfield(),
              SizedBox(
                height: 16,
              ),
              AppTextStyle.getNunitoRegular(
                  'Customer Name', 14, AppColors.kPrimaryColor),
              SizedBox(
                height: 8,
              ),
              getCustomerNameTextfield(),
              SizedBox(
                height: 16,
              ),
              AppTextStyle.getNunitoRegular(
                  'Contact Number', 14, AppColors.kPrimaryColor),
              SizedBox(
                height: 8,
              ),
              getContactNumberTextfield(),
              SizedBox(
                height: 16,
              ),
              AppTextStyle.getNunitoRegular(
                  'GSTIN Number', 14, AppColors.kPrimaryColor),
              SizedBox(
                height: 8,
              ),
              getGSTNumberTextfield(),
              Spacer(),
              getOnboardButton(),
              SizedBox(
                height: 50,
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget getOnboardButton() {
    return Style.getButtonWithRoundedCorners(Get.width, 40.0, () {},
        AppTextStyle.getNunitoBold('Onboard', 16, AppColors.FFFFFFFF),
        backgroundColor: AppColors.kPrimaryDark,
        leftMargin: 0,
        borderColor: Colors.transparent,
        rightMargin: 0);
  }

  Widget getCompanyNameTextfield() {
    return Style.getInputTextField(
      'Enter Company Name',
      TextInputType.emailAddress,
      TextEditingController(),
      Get.width,
      prefixIcon: const Icon(
        Icons.local_convenience_store_rounded,
        color: AppColors.FF808080,
      ),
      leftMargin: 0,
      rightMargin: 0,
      onChanged: (value) {},
      // outlineBorderColor: !controller.showEmailError.value
      //     ? AppColors.kPrimaryDark
      //     : AppColors.FFF50031,
    );
  }

  Widget getContactNumberTextfield() {
    return Style.getInputTextField(
      'Enter Contact Number',
      TextInputType.emailAddress,
      TextEditingController(),
      Get.width,
      prefixIcon: const Icon(
        Icons.phone,
        color: AppColors.FF808080,
      ),
      leftMargin: 0,
      rightMargin: 0,
      onChanged: (value) {},
      // outlineBorderColor: !controller.showEmailError.value
      //     ? AppColors.kPrimaryDark
      //     : AppColors.FFF50031,
    );
  }

  Widget getCustomerNameTextfield() {
    return Style.getInputTextField(
      'Enter Customer Name',
      TextInputType.emailAddress,
      TextEditingController(),
      Get.width,
      prefixIcon: const Icon(
        Icons.person,
        color: AppColors.FF808080,
      ),
      leftMargin: 0,
      rightMargin: 0,
      onChanged: (value) {},
      // outlineBorderColor: !controller.showEmailError.value
      //     ? AppColors.kPrimaryDark
      //     : AppColors.FFF50031,
    );
  }

  Widget getGSTNumberTextfield() {
    return Style.getInputTextField(
      'Enter GSTIN Number',
      TextInputType.number,
      TextEditingController(),
      Get.width,
      prefixIcon: const Icon(
        Icons.numbers_outlined,
        color: AppColors.FF808080,
      ),
      leftMargin: 0,
      rightMargin: 0,
      onChanged: (value) {},
      // outlineBorderColor: !controller.showEmailError.value
      //     ? AppColors.kPrimaryDark
      //     : AppColors.FFF50031,
    );
  }
}
