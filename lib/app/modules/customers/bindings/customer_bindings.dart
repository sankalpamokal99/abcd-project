import 'package:get/get.dart';

import '../controller/customers_controller.dart';

class CustomersBinding extends Bindings {
  @override
  void dependencies() {
    Get.put(CustomersController());
  }
}
