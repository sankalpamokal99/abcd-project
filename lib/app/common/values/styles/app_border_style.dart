import 'package:abcd_enterprises/app/common/util/extensions.dart';
import 'package:flutter/material.dart';

import '../app_colors.dart';

class AppBorderStyle {
  const AppBorderStyle._();

  static final InputBorder editTextBorder = UnderlineInputBorder(
      borderRadius: 0.borderRadius,
      borderSide: BorderSide(color: AppColors.FF000000.withOpacity(0.65)));
  static final InputBorder editTextBorderEnabled = UnderlineInputBorder(
      borderRadius: 0.borderRadius,
      borderSide: BorderSide(color: AppColors.FF000000.withOpacity(0.65)));

  static final InputBorder editTextBorderFP = UnderlineInputBorder(
      borderRadius: 0.borderRadius,
      borderSide: const BorderSide(color: AppColors.white));
  static const InputBorder underLineInputBorder = UnderlineInputBorder(
      borderSide:
          BorderSide(color: AppColors.dropdownUnderlineColor, width: 2.0));
  static final InputBorder outlineInputBorder = OutlineInputBorder(
      borderRadius: 8.borderRadius,
      borderSide: const BorderSide(color: AppColors.FFCFD3DF));

  static const ShapeBorder bottomSheetBorderShape = RoundedRectangleBorder(
    borderRadius: BorderRadius.only(
        topRight: Radius.circular(24), topLeft: Radius.circular(24)),
  );
}
