class FontFamily {
  const FontFamily._();
  //
  // static const String JioTypeBlack = 'JioTypeBlack';
  // static const String JioTypeBold = 'JioTypeBold';
  // static const String JioTypeLight = 'JioTypeLight';
  // static const String JioTypeMedium = 'JioTypeMedium';
  static const String NunitoBold = 'NunitoBold';
  static const String NunitoLight = 'NunitoLight';
  static const String NunitoMedium = 'NunitoMedium';
  static const String NunitoRegular = 'NunitoRegular';
}
