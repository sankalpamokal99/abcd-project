import 'package:flutter/material.dart';

import '../app_colors.dart';
import 'dimens.dart';
import 'font_family.dart';

class AppTextStyle {
  const AppTextStyle._();

  static final TextStyle semiBoldStyle = _textStyle.copyWith(
      fontSize: Dimens.fontSize16,
      fontWeight: FontWeight.w700,
      fontFamily: FontFamily.NunitoBold);

  static final TextStyle boldStyle = _textStyle.copyWith(
      fontSize: Dimens.fontSize22,
      fontWeight: FontWeight.w700,
      fontFamily: FontFamily.NunitoBold);

  static TextStyle regularStyle = _textStyle.copyWith(
      fontSize: Dimens.fontSize14,
      fontWeight: FontWeight.w500,
      fontFamily: FontFamily.NunitoMedium,
      color: AppColors.FF000000.withOpacity(0.65));

  static TextStyle lightStyle = _textStyle.copyWith(
    fontSize: Dimens.fontSize16,
    fontWeight: FontWeight.w300,
    fontFamily: FontFamily.NunitoLight,
    color: AppColors.FF707070,
  );

  static final TextStyle buttonTextStyle = _textStyle.copyWith(
    fontSize: Dimens.fontSize16,
    fontWeight: FontWeight.w700,
    fontFamily: FontFamily.NunitoBold,
  );

  static final TextStyle hintTextStyleMobile = TextStyle(
    color: AppColors.FFFFFFFF.withOpacity(0.6),
    fontSize: Dimens.fontSize16,
    fontFamily: FontFamily.NunitoLight,
  );

  static const TextStyle _textStyle = TextStyle(
    color: Colors.black,
    fontSize: Dimens.fontSize14,
    fontFamily: FontFamily.NunitoBold,
  );

  static const TextStyle hintTextStyle = TextStyle(
    color: Colors.black26,
    fontSize: Dimens.fontSize14,
    fontFamily: FontFamily.NunitoLight,
  );

  static const TextStyle hintFocusTextStyle = TextStyle(
    color: AppColors.FF1E7B74,
    fontSize: Dimens.fontSize14,
    fontFamily: FontFamily.NunitoMedium,
  );

  static const TextStyle inputTextStyleMobile = TextStyle(
    color: AppColors.FFFFFFFF,
    fontSize: Dimens.fontSize16,
    fontFamily: FontFamily.NunitoMedium,
  );

  static const TextStyle inputTextStyleMobileBottomsheet = TextStyle(
    color: AppColors.dropdownUnderlineColor,
    fontSize: Dimens.fontSize16,
    fontFamily: FontFamily.NunitoMedium,
  );

  static const TextStyle hintTextStyleMobileBottomsheet = TextStyle(
    color: AppColors.dropdownUnderlineColor,
    fontSize: Dimens.fontSize16,
    fontFamily: FontFamily.NunitoMedium,
  );

  static Widget getNunitoMedium(String text, double size, Color color,
      {TextAlign align = TextAlign.start,
      int maxLines = 1,
      TextOverflow overflow = TextOverflow.ellipsis}) {
    return Text(
      text,
      textAlign: align,
      maxLines: maxLines,
      overflow: overflow,
      style: TextStyle(
          color: color,
          fontSize: size,
          fontWeight: FontWeight.w500,
          fontFamily: FontFamily.NunitoMedium,
          decoration: TextDecoration.none),
    );
  }

  static Widget getNunitoLight(String text, double size, Color color,
      {TextAlign align = TextAlign.start,
      int maxLines = 1,
      TextOverflow overflow = TextOverflow.ellipsis}) {
    return Text(
      text,
      textAlign: align,
      maxLines: maxLines,
      overflow: overflow,
      style: TextStyle(
          color: color,
          fontSize: size,
          fontWeight: FontWeight.w300,
          fontFamily: FontFamily.NunitoLight,
          decoration: TextDecoration.none),
    );
  }

  static Widget getNunitoRegular(String text, double size, Color color,
      {TextAlign align = TextAlign.start,
      int maxLines = 1,
      TextOverflow overflow = TextOverflow.ellipsis}) {
    return Text(
      text,
      textAlign: align,
      maxLines: maxLines,
      overflow: overflow,
      style: TextStyle(
          color: color,
          fontSize: size,
          fontWeight: FontWeight.w500,
          fontFamily: FontFamily.NunitoRegular,
          decoration: TextDecoration.none),
    );
  }

  static Widget getNunitoBold(String text, double size, Color color,
      {TextAlign align = TextAlign.start,
      int maxLines = 1,
      TextOverflow overflow = TextOverflow.ellipsis}) {
    return Text(
      text,
      textAlign: align,
      maxLines: maxLines,
      overflow: overflow,
      style: TextStyle(
          color: color,
          fontSize: size,
          fontWeight: FontWeight.w700,
          fontFamily: FontFamily.NunitoBold,
          decoration: TextDecoration.none),
    );
  }
}
