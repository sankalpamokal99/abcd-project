class Constants {
  const Constants._();
  static const String somethingWrong =
      "Oops something went wrong please try after sometime";
  static const String connectionTimeout = "connection timeout";
  static const String noConnection = "no connection";
  static const String noInternetConnection =
      "Please check your internet connection";
  static const String youAreOffline = "You are Offline";
  static const String unAuthorized = "Invalid username or password.";
}
