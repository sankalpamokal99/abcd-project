abstract class ConfigMap {
  static String ticketsCreateTicket = "tickets/createTicket";
  static String ticketNotesAddNote = "ticketNotes/addNote";
  static String ticketsUpdateTransStatus = "tickets/updateTransStatus";
  static String ticketsUpdateTransAssigned = "tickets/updateTransAssigned";
  static String ticketsUpdatePriority = "tickets/updatePriority";
  static String ticketsUpdateGroup = "tickets/updateGroup";
  static String ticketNotesUpdate = "ticketNotes/update";
  static String ticketsDeleteTicket = "tickets/deleteTicket";
  static String ticketsExport = "tickets/export";
  static String settingsView = "settings/view";
  static String manageUsers = "manage/users";
  static String manageBusinessHours = "manage/bussinessHours";
  static String manageSla = "manage/sla";
  static String manageRoles = "manage/roles";
  static String manageGroups = "manage/groups";
  static String manageEmailAutomation = "manage/emailAutomation";
  static String manageAgentShift = "manage/agentShift";
  static String manageTicketCustomization = "manage/ticketCustomization";
  static String manageContactCustomization = "manage/contactCustomization";
  static String customerView = "customer/view";
  static String contactsDelete = "contacts/delete";
  static String contactsManage = "contacts/manage";
  static String customerSegmentationManage = "customerSegmentation/manage";
  static String manageTags = "manage/tags";
  static String editTicketAll = "edit/ticketAll";
  static String ticketsUpdateSubjectDescription = "tickets/updateSubjectDescription";

}
