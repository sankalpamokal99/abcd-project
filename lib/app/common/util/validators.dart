import 'package:get/get.dart';

import '../values/strings.dart';

class Validators {
  Validators._();

  static final RegExp validDecimal = RegExp(r'^[-+]?\d*\.?\d{0,2}');
  static final RegExp validNumber = RegExp(r'^[-+]?\d*');

  static String? validateEmpty(String? v) {
    if (v == null || v.isEmpty || (v.trim().isEmpty)) {
      return Strings.fieldCantBeEmpty;
    } else {
      return null;
    }
  }

  static String? validateLastName(String? v) {
    if (v == null || v.isEmpty || (v.trim().isEmpty)) {
      return Strings.fieldCannotBeEmpty;
    } else {
      return null;
    }
  }

  static String? validateTEmpty<T>(T? v) {
    if (v == null) {
      return Strings.fieldCantBeEmpty;
    } else {
      return null;
    }
  }

  static String? validateEmail(String? v) {
    if (v == null || v.isEmpty) {
      return Strings.emailCantBeEmpty;
    } else if (!GetUtils.isEmail(v)) {
      return Strings.enterValidEmail;
    } else {
      return null;
    }
  }

  //ignores empty string
  static String? validateEmail2(String? v) {
    if (v == null || v.isEmpty) {
      return null;
    } else if (!GetUtils.isEmail(v)) {
      return Strings.enterValidEmail;
    } else {
      return null;
    }
  }

  static String? validatePhone(String? v) {
    if (v!.isEmpty) {
      return Strings.fieldCantBeEmpty;
    } else if (!GetUtils.isPhoneNumber(v) || (!(v.length == 10))) {
      return Strings.enterValidNumber;
    } else {
      return null;
    }
  }

  static String? validateOnlyPhone(String? v) {
    if (v == null) {
      return null;
    }
    if (GetUtils.isPhoneNumber(v) == false || (!(v.length == 10))) {
      return Strings.enterValidNumber;
    } else {
      return null;
    }
  }

  static String? validateEmailPhone(String? v) {
    if (v!.isEmpty) {
      return Strings.fieldCantBeEmpty;
    } else if (GetUtils.isNumericOnly(v)) {
      return validatePhone(v);
    } else {
      return validateEmail(v);
    }
  }

  static String? validatePasswordPolicy(String? v) {
    final RegExp validPassword = RegExp(
        r"^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*()<>{}[\]]).{8,}$");
    if (v!.isEmpty) {
      return "${Strings.password} ${Strings.cantBeEmpty}";
    } else if (!(validPassword.hasMatch(v))) {
      return Strings.passwordPolicyError;
    } else {
      return null;
    }
  }

  static String? validateNewPassword(String? v, String password) {
    final RegExp validPassword = RegExp(
        r"^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*()<>{}[\]]).{8,}$");
    if (v!.isEmpty) {
      return "${Strings.password} ${Strings.cantBeEmpty}";
    } else if (!(validPassword.hasMatch(v))) {
      return Strings.passwordPolicyError;
    } else if (v.compareTo(password) == 0) {
      return Strings.newPasswordValidation;
    } else {
      return null;
    }
  }

  static String? validateConfirmPassword(String? v, String password) {
    if (v!.compareTo(password) != 0) {
      return Strings.confirmPasswordValidation;
    } else {
      return null;
    }
  }

  static String? validateCheckbox({
    bool v = false,
    String error = Strings.checkboxValidation,
  }) {
    if (!v) {
      return error;
    } else {
      return null;
    }
  }

  static String? validateName(String? v) {
    if (v!.isEmpty) {
      return "${Strings.contactName} ${Strings.cantBeEmpty}";
    } else if (v.contains(RegExp(r'[0-9]'))) {
      return "${Strings.enterValid} ${Strings.contactName}";
    } else if (v.substring(0, 1).contains(" ")) {
      return "${Strings.enterValid} ${Strings.contactName}";
    } else {
      return null;
    }
  }

  static String? validateName2(String? v) {
    if (v!.isEmpty) {
      return "Field ${Strings.cantBeEmpty}";
    } else if (v.contains(RegExp(r'[0-9]'))) {
      return "Field cannot contain number";
    } else if (v.substring(0, 1).contains(" ")) {
      return "Field cannot start with space";
    } else {
      return null;
    }
  }

  static String? validateNumberOrDecimal(String? v,
      {bool isDecimal = false, isRequired = false}) {
    if (isRequired) {
      if (v!.isEmpty) {
        return "This field ${Strings.cantBeEmpty}";
      } else if (v == '-') {
        return isDecimal
            ? "Please enter valid decimal  number"
            : "Please enter valid number";
      } else if (isDecimal && !(validDecimal.hasMatch(v))) {
        return "Please enter valid decimal number";
      } else if (!isDecimal && !(validNumber.hasMatch(v))) {
        return "Please enter valid number";
      } else {
        return null;
      }
    } else {
      if (v == '-') {
        isDecimal
            ? "Please enter valid decimal  number"
            : "Please enter valid number";
      } else if (isDecimal && !(validDecimal.hasMatch(v!)) && v.isNotEmpty) {
        return "Please enter valid decimal number";
      } else if (!isDecimal && !(validNumber.hasMatch(v!)) && v.isNotEmpty) {
        return "Please enter valid number";
      } else {
        return null;
      }
    }
    return null;
  }
}
