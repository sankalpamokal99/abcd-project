import 'dart:async';

import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';

class Initializer {
  static const Initializer instance = Initializer._internal();
  factory Initializer() => instance;
  const Initializer._internal();

  void init(VoidCallback runApp) {
    ErrorWidget.builder = (errorDetails) {
      return Text(
        errorDetails.exceptionAsString(),
      );
    };

    runZonedGuarded(() async {
      WidgetsFlutterBinding.ensureInitialized();

      // Screen Orientations
      SystemChrome.setPreferredOrientations(
          [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);

      FlutterError.onError = (details) {
        FlutterError.dumpErrorToConsole(details);
        printInfo(
            info:
                "============Flutter error========= ${details.stack.toString()}");
      };

      // await setEnvironment();
      //print("cookies : ${ClientCookie.getCookie()}");

      runApp();
    }, (error, stack) {
      printInfo(info: 'runZonedGuarded: ${error.toString()}');
    });
  }
}

class InitialBindings extends Bindings {
  @override
  void dependencies() {}
}
