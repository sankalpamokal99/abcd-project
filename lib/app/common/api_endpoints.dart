class ApiEndPoints {
  const ApiEndPoints._();

  static String login = "/login";
  static String logout = "/logout";
}

class Environment {
  static const ENVIRONMENT = "config/environment.json";
  static const DEV = "config/dev.json";
  static const QA = "config/qa.json";
  static const PPD = "config/preprod.json";
  static const PROD = "config/prod.json";
}
