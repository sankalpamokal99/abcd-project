// import 'package:get_storage/get_storage.dart';
//
// class Storage {
//   const Storage._();
//
//   static final GetStorage _storage = GetStorage();
//
//   static GetStorage get storage => _storage;
//
//   static Future<void> saveValue(String key, dynamic value) =>
//       _storage.write(key, value);
//
//   static T? getValue<T>(String key) => _storage.read<T>(key);
//
//   static bool hasData(String key) => _storage.hasData(key);
//
//   static Future<void> removeValue(String key) => _storage.remove(key);
//
//   static Future<void> clearStorage() => _storage.erase();
//
//   static setLogin(bool value) => _storage.write("IS_LOGIN", value);
//   static bool isLogin() => _storage.read<bool>("IS_LOGIN") ?? false;
//
//   static setUserID(String value) => _storage.write("USER_ID", value);
//   static String? getUserID() => _storage.read<String>("USER_ID");
//
//   static setFirstName(String value) => _storage.write("FIRST_NAME", value);
//   static String? getFirstName() => _storage.read<String>("FIRST_NAME");
//
//   static setLastName(String value) => _storage.write("LAST_NAME", value);
//   static String? getLastName() => _storage.read<String>("LAST_NAME");
//
//   static setToken(String value) => _storage.write("TOKEN", value);
//   static String? getToken() => _storage.read<String>("TOKEN") ?? "";
//
//   static setUserType(String value) => _storage.write("USERTYPE", value);
//   static String? getUserType() => _storage.read<String>("USERTYPE") ?? "";
//
//   static setTenantId(String value) => _storage.write("TENANTID", value);
//   static String? getTenantId() => _storage.read<String>("TENANTID") ?? "auth";
//
//   static setConfigData(List<String> value) =>
//       _storage.write("CONFIG_DATA", value);
//   static List<dynamic> getConfigData() =>
//       _storage.read<List<dynamic>>("CONFIG_DATA") ?? List<dynamic>.empty();
//
//   static setBaseUrl(String? value) => _storage.write("BASE_URL", value ?? "");
//   static String? getBaseUrl() => _storage.read<String>("BASE_URL") ?? "";
// }
