part of 'app_pages.dart';

abstract class Routes {
  static const LOGIN_PAGE = _Paths.LOGIN;
  static const SPLASH_PAGE = _Paths.SPLASH;
  static const HOME_PAGE = _Paths.HOME;
  static const MAIN_PAGE = _Paths.MAIN_PAGE;
  static const ADD_CUSTOMER_PAGE = _Paths.ADD_CUSTOMER;

  ///Menu
  static const BOTTOM_HOME = "/home";
  static const BOTTOM_PRODUCTS = "/products";
  static const BOTTOM_OREDERS = "/orders";
  static const BOTTOM_PROFILE = "/profile";
  static const BOTTOM_CUSTOMERS = "/customers";
}

abstract class _Paths {
  static const SPLASH = '/splash';
  static const LOGIN = '/login';
  static const MAIN_PAGE = '/main';
  static const HOME = '/home';
  static const ADD_CUSTOMER = '/add_customer';
}
