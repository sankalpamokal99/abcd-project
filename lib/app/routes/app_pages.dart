import 'package:abcd_enterprises/app/modules/customers/view/add_new_customer.dart';
import 'package:get/get.dart';

import '../modules/home/bindings/home_bindings.dart';
import '../modules/home/view/home_page.dart';
import '../modules/layout_template/bindings/main_binding.dart';
import '../modules/layout_template/view/main_page.dart';
import '../modules/login/bindings/login_bindings.dart';
import '../modules/login/view/login_page.dart';
import '../modules/splash/bindings/splash_bindings.dart';
import '../modules/splash/view/splash_page.dart';

part 'app_routes.dart';

class AppPages {
  const AppPages._();
  static final routes = [
    GetPage(
        name: Routes.SPLASH_PAGE,
        page: () => const SplashPage(),
        bindings: [SplashBinding()]),
    GetPage(
        name: Routes.LOGIN_PAGE,
        page: () => const LoginPage(),
        binding: LoginBinding()),
    GetPage(
      name: Routes.MAIN_PAGE,
      binding: MainBinding(),
      page: () => LayoutTemplate(),
    ),
    GetPage(
        name: Routes.BOTTOM_HOME,
        page: () => const HomePage(),
        binding: HomeBinding()),
    GetPage(
      name: Routes.ADD_CUSTOMER_PAGE,
      page: () => const AddNewCustomer(),
    ),
  ];
}
